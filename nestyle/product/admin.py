from django.utils.safestring import mark_safe
from django.contrib import admin
from . import models

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    list_display_links = ['id', 'name']

@admin.register(models.Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    list_display_links = ['id', 'name']

@admin.register(models.Color)
class ColorAdmin(admin.ModelAdmin):
    def thumbnail(self, obj):
        """
        썸네일 미리보기를 관리자 페이지에서 보여줍니다.
        """
        return mark_safe('<div style="width:50px;height:50px;background-color: ' + obj.hexcode + ';"></div>')
    
    thumbnail.short_description = "Thumbnail"

    list_display = ['id', 'name', 'thumbnail']
    list_display_links = ['id', 'name']

class ProductImageInline(admin.TabularInline):
    model = models.ProductImages

@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ProductImageInline,
    ]
    """
    카테고리 관리자입니다.
    """

    def thumbnail(self, obj):
        """
        썸네일 미리보기를 관리자 페이지에서 보여줍니다.
        """
        return mark_safe('<img src="{}" style="width:100px;"/>'.format(obj.image.url))
    
    thumbnail.short_description = "Thumbnail"

    def get_keywords(self, obj):
        return ', '.join([k.value for k in obj.keywords.all()])

    list_display = ['thumbnail', 'id',  'brand', 'category', 'name','get_keywords', 'create_date']
    list_display_links = ['id', 'name']


@admin.register(models.Keyword)
class KeywordAdmin(admin.ModelAdmin):
    """
    키워드 관리자입니다.
    """
    list_display = ['value']

@admin.register(models.Recommandation)
class RecommandationAdmin(admin.ModelAdmin):
    """
    추천관 관리자입니다.
    """
    def thumbnail(self, obj):
        return mark_safe('<img src="{}" style="width:100px;"/>'.format(obj.image.url))
    thumbnail.short_description = "Thumbnail"
    list_display = ['thumbnail', 'url', 'name']
