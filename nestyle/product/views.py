from django.contrib.auth.models import User
from django.http.response import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from . import models

class Thumbnail(APIView):
    """
    상품 썸네일을 출력합니다.
    """
    permission_classes = [permissions.AllowAny]

    def get(self, request, product_id, *arg, **kwargs):
        pass


class Category(APIView):
    """
    카테고리, 누구나 접근 가능합니다.
    """
    permission_classes = [permissions.AllowAny]

    def get(self, request, *arg, **kwargs):
        """
        모든 카테고리 목록을 반환합니다.

        :return -> list: [{id: 고유번호(pk), name: 카테고리명}, ...]
        """
        rows = []
        for category in models.Category.objects.all():
            rows.append({
                "id": category.id,
                "name": category.name
            })
        return JsonResponse({
            "status": True,
            "data": rows
        })
        


class Brand(APIView):
    """
    브랜드, 누구나 접근 가능합니다.
    """
    permission_classes = [permissions.AllowAny]

    def get(self, request, *arg, **kwargs):
        """
        모든 브랜드 목록을 반환합니다.

        :return -> list: [{id: 고유번호(pk), name: 브랜드명}, ...]
        """
        rows = []
        for brand in models.Brand.objects.all():
            rows.append({
                "id": brand.id,
                "name": brand.name
            })
        return JsonResponse({
            "status": True,
            "data": rows
        })

class ProductList(APIView):
    """
    상품리스트, 누구나 접근가능합니다.
    """
    permission_classes = [permissions.AllowAny]

    def get(self, request, category_id, *args, **kwargs):
        """
        카테고리 번호를 확인하고 결과를 반환합니다.
        결과가 없거나 카테고리가 잘못된경우 data: [] 를 반환하며
        카테고리가 잘못된 경우 status: False를 반환하비다.
        """

        offset = request.GET.get("offset", 0)
        size = request.GET.get("size", 8)

        try:
            offset = int(offset)
            size = int(size)
        except:
            offset = 0
            size = 8

        try:
            category = models.Category.objects.get(id=category_id)
        except models.Category.DoesNotExist:
            return JsonResponse({
                "status": False,
                "message": "존재하지 않는 카테고리입니다.",
                "data": []
            })

        products = models.Product.objects.filter(category=category)[offset:offset+size]
        rows = []
        for p in products:
            rows.append({
                "id": p.id,
                "category_id": p.category.id,
                "brand_id": p.brand.id,
                "name": p.name,
                "image": p.image.url,
                "create_date": p.create_date
            })
        return JsonResponse({
            "status": True,
            "data": rows
        })



class ProductListBrand(APIView):
    """
    상품리스트, 누구나 접근가능합니다.
    """
    permission_classes = [permissions.AllowAny]

    def get(self, request, brand_id, *args, **kwargs):
        """
        카테고리 번호를 확인하고 결과를 반환합니다.
        결과가 없거나 카테고리가 잘못된경우 data: [] 를 반환하며
        카테고리가 잘못된 경우 status: False를 반환하비다.
        """

        offset = request.GET.get("offset", 0)
        size = request.GET.get("size", 8)

        try:
            offset = int(offset)
            size = int(size)
        except:
            offset = 0
            size = 8

        try:
            brand = models.Brand.objects.get(id=brand_id)
        except models.Brand.DoesNotExist:
            return JsonResponse({
                "status": False,
                "message": "존재하지 않는 카테고리입니다.",
                "data": []
            })

        products = models.Product.objects.filter(brand=brand)[offset:offset+size]
        rows = []
        for p in products:
            rows.append({
                "id": p.id,
                "category_id": p.category.id,
                "brand_id": p.brand.id,
                "name": p.name,
                "image": p.image.url,
                "create_date": p.create_date
            })
        return JsonResponse({
            "status": True,
            "data": rows
        })

