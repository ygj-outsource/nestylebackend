# Generated by Django 3.2.7 on 2021-10-21 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0014_defaultmodel_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='defaultmodel',
            name='image_path',
            field=models.ImageField(upload_to='uploaded/model_images/', verbose_name='이미지'),
        ),
        migrations.AlterField(
            model_name='hair',
            name='image',
            field=models.ImageField(upload_to='uploaded/hair/', verbose_name='이미지'),
        ),
    ]
