from . import views
from django.urls import path, include


urlpatterns = [
    path('category/', views.Category.as_view()),
    path('brand/', views.Brand.as_view()),
    path('thumbnail/<product_id>', views.Thumbnail.as_view()),
    path('list/<category_id>', views.ProductList.as_view()),
    path('brand/<brand_id>', views.ProductListBrand.as_view()),
]