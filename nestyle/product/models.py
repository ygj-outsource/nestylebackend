from django.db import models

class Brand(models.Model):
    BRAND_TYPE_CHOICES = (
        ('BRAND', '브랜드'),
        ('LUXURY', '명품'),
    )
    name = models.CharField("브랜드 명", max_length=100)
    logo = models.ImageField("로고", upload_to="uploaded/brand_logos/")
    brand_type = models.CharField('브랜드 타입', max_length=64, choices=BRAND_TYPE_CHOICES, default='BRAND')
    def __str__(self):
        return self.name

class Recommandation(models.Model):
    url = models.URLField('링크')
    image = models.ImageField("이미지", upload_to="uploaded/recommandations/")
    name = models.CharField(max_length=128)
    def __str__(self):
        return '{}({})'.format(self.url, self.name)

class Category(models.Model):
    name = models.CharField("카테고리 명", max_length=100)
    def __str__(self):
        return self.name

class Color(models.Model):
    name = models.CharField("색이름", max_length=64)
    hexcode = models.CharField("헥스코드", max_length=7, default='#000000') # #aabbcc 형태
    def __str__(self):
        return f"{self.name}[{self.hexcode}]"

class Keyword(models.Model):
    value = models.CharField("키워드", max_length=64)
    def __str__(self):
        return self.value

class Product(models.Model):
    category = models.ForeignKey('product.Category', on_delete=models.DO_NOTHING, related_name='products')
    brand = models.ForeignKey('product.Brand', on_delete=models.DO_NOTHING, related_name='products')
    name = models.CharField("상품명", max_length=100)
    image = models.ImageField("이미지", upload_to="uploaded/", help_text="모델을 제외한 AI처리에 사용될 옷 사진입니다.")
    front_image = models.ImageField("대표 이미지", upload_to="uploaded/", help_text="대표로 보여질 사진입니다.", blank=True)
    additional_image1 = models.ImageField("추가이미지 1", upload_to="uploaded/", help_text="상단에 보여질 추가 대표 이미지입니다.", blank=True)
    additional_image2 = models.ImageField("추가이미지 2", upload_to="uploaded/", help_text="상단에 보여질 추가 대표 이미지입니다.", blank=True)
    create_date = models.DateTimeField("작성일", auto_now=True)
    color = models.ManyToManyField("Color")
    price = models.IntegerField("가격", default=0)
    click_count = models.IntegerField("클릭카운트", default=0)
    suggest = models.IntegerField("추천 정도", default=0)
    keywords = models.ManyToManyField(Keyword)
    url = models.URLField('링크', default='')
    description = models.CharField("설명", max_length=8192, default='')

    def __str__(self):
        return f"[{self.brand.name}({self.category.name})]{self.name}"
    def images(self):
        return ProductImages.objects.filter(product = self)

class ProductImages(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='uploaded/product_images/')

class DefaultModel(models.Model):
    name = models.CharField("모델이름", max_length=100)
    image_path = models.ImageField("이미지", upload_to="uploaded/model_images/")
    description = models.CharField("모델설명", max_length=1024, default='')
    def __str__(self):
        return f"{self.name}({self.description})"

class Hair(models.Model):
    name = models.CharField("헤어스타일명", max_length=100)
    image = models.ImageField("이미지", upload_to="uploaded/hair/")
    create_date = models.DateTimeField("작성일", auto_now=True)
    def __str__(self):
        return f"{self.name}"