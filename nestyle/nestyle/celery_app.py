import os

from celery import Celery
from django.conf import settings
import os
# Set default Django settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nestyle.settings')

brokers = [f'amqp://{settings.CELERY_RABBITMQ_USER}:{settings.CELERY_RABBITMQ_PASSWORD}@{host}//' 
		for host in settings.CELERY_RABBITMQ_HOSTS]
app = Celery('nestyle', broker=brokers, 
		broker_transport_options={'confirm_publish': True})

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()