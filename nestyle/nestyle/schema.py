import graphene
import main.models
import product.models
import urllib.parse
from django.db.models import Q
from graphene_django.types import DjangoObjectType

from graphene import relay, ObjectType
from graphene_django.filter import DjangoFilterConnectionField


class ModelFile(DjangoObjectType):
    class Meta:
        model = main.models.ModelFile

class BrandType(DjangoObjectType):
    class Meta:
        model = product.models.Brand
        exclude = []

    def resolve_logo(self, info):
        return self.logo.url


class CategoryType(DjangoObjectType):
    class Meta:
        model = product.models.Category
        exclude = []

class ColorType(DjangoObjectType):
    class Meta:
        model = product.models.Color
        exclude = []
class ImageType(DjangoObjectType):
    class Meta:
        model = product.models.ProductImages
        exclude = []
        def resolve_image(self, info):
            return self.image.url

class ProductNode(DjangoObjectType):
    class Meta:
        model = product.models.Product
        filter_fields=[]
        interfaces = (relay.Node, )
    id = graphene.ID(source='pk', required=True)
    image = graphene.String()
    images = graphene.List(ImageType)
    front_image = graphene.String()
    additional_image1 = graphene.String()
    additional_image2 = graphene.String()
    
    def resolve_image(self, info):
        return self.image.url
    def resolve_front_image(self, info):
        try:
            return self.front_image.url
        except:
            return ''
    def resolve_additional_image1(self, info):
        try:
            return self.additional_image1.url
        except:
            return ''
    def resolve_additional_image2(self, info):
        try:
            return self.additional_image2.url
        except:
            return ''

    def resolve_images(self, info):
        try:
            return self.images()
        except:
            return []

class HairType(DjangoObjectType):
    class Meta:
        model = product.models.Hair
        exclude = []

class RecommandationType(DjangoObjectType):
    class Meta:
        model = product.models.Recommandation
        exclude = []

class KeywordType(DjangoObjectType):
    class Meta:
        model = product.models.Keyword
        exclude = []

class ByBrandType(ObjectType):
    brand = graphene.Field(BrandType)
    products = DjangoFilterConnectionField(ProductNode)

class ByCategoryType(ObjectType):
    category = graphene.Field(CategoryType)
    products = DjangoFilterConnectionField(ProductNode)

class ByQueryType(ObjectType):
    products = DjangoFilterConnectionField(ProductNode)

def build_qs(qs, query):
    name = None
    color = None
    brand = None
    category = None
    keywords = None
    if 'name' in query:
        name = query['name'][0]
    if 'color' in query:
        color = query['color'][0]
    if 'brand' in query:
        brand = query['brand'][0]
    if 'category' in query:
        category = query['category'][0]
    if 'keywords' in query:
        keywords = query['keywords']

    if name:
        qs = qs.filter(name__contains=name)
    if color:
        qs = qs.filter(color__hexcode=color)
    if brand:
        qs = qs.filter(brand__id=brand)
    if category:
        qs = qs.filter(category__id=category)
    if keywords:
        for keyword in keywords:
            qs = qs.filter(keywords__value__contains=keyword)

    return qs.order_by('-create_date')

class Query(graphene.ObjectType):
    all_brands = graphene.List(BrandType)
    all_category = graphene.List(CategoryType)
    all_color = graphene.List(ColorType)
    all_product = graphene.List(ProductNode)
    all_hair = graphene.List(HairType)
    all_model_files = graphene.List(ModelFile)
    all_hair_model_files = graphene.List(ModelFile)
    all_recommandations = graphene.List(RecommandationType)
    all_keywords = graphene.List(KeywordType)
    all_sample_clothes = graphene.List(ModelFile)
    product = graphene.Field(ProductNode, id=graphene.String())
    product_by_brand = graphene.List(ByBrandType, query=graphene.String())
    product_by_category = graphene.List(ByCategoryType, query=graphene.String())


    product_by_name = graphene.List(ProductNode, name=graphene.String(required=True))
    product_by_color = graphene.List(ProductNode, hexcode=graphene.String(required=True))
    product_by_filter = graphene.List(ProductNode, hexcode=graphene.String(required=True))
    product_by_query = graphene.Field(ByQueryType, query=graphene.String())
    def resolve_product(root, info, id):
        return product.models.Product.objects.get(id=id)
    def resolve_all_brands(root, info):
        return product.models.Brand.objects.all()
    def resolve_all_recommandations(root, info):
        return product.models.Recommandation.objects.all()

    def resolve_all_keywords(root, info):
        return product.models.Keyword.objects.all()

    def resolve_all_category(root, info):
        return product.models.Category.objects.all()

    def resolve_all_color(root, info):
        return product.models.Color.objects.all()

    def resolve_all_product(root, info):
        return product.models.Product.objects.all()

    def resolve_all_hair(root, info):
        return product.models.Hair.objects.all()

    def resolve_all_model_files(root, info):
        return main.models.ModelFile.objects.filter(Q(model_type='MODEL') | Q(model_type='HAIR_MODEL'))
    def resolve_all_hair_model_files(root, info):
        return main.models.ModelFile.objects.filter(model_type='HAIR_MODEL')

    def resolve_all_sample_clothes(root, info):
        return main.models.ModelFile.objects.filter(model_type='CLOTH')

    def resolve_product_by_brand(root, info, query):
        ret = []
        query = urllib.parse.parse_qs(query)
        for brand in product.models.Brand.objects.all():
            item = ByBrandType()
            qs = build_qs(brand.products, query)
            if len(qs) == 0:
                continue

            item.products = qs
            item.brand = brand
            ret.append(
                item
            )
        return ret


    
    def resolve_product_by_category(root, info, query):
        ret = []
        query = urllib.parse.parse_qs(query)
        for category in product.models.Category.objects.all():
            item = ByCategoryType()
            qs = build_qs(category.products, query)
            if len(qs) == 0:
                continue
            item.products = qs
            item.category = category
            ret.append(
                item
            )
        return ret

    def resolve_product_by_query(root, info, query):
        ret = ByQueryType()
        query = urllib.parse.parse_qs(query)
        qs = product.models.Product.objects
        ret.products = build_qs(qs, query)
        return ret
        # name=xx&color=#aabbcc

schema = graphene.Schema(query=Query)



