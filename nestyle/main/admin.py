from django.contrib import admin
from django.utils.safestring import mark_safe
from main import models


from django_celery_results.models import TaskResult, GroupResult

admin.site.unregister(GroupResult)

from django_celery_beat.models import PeriodicTask, ClockedSchedule, CrontabSchedule, IntervalSchedule, SolarSchedule

admin.site.unregister(PeriodicTask)
admin.site.unregister(ClockedSchedule)
admin.site.unregister(CrontabSchedule)
admin.site.unregister(IntervalSchedule)
admin.site.unregister(SolarSchedule)

from django.contrib.auth.models import User, Group

admin.site.unregister(User)
admin.site.unregister(Group)

@admin.register(models.ModelFile)
class ModelFileAdmin(admin.ModelAdmin):
    """
    모델파일 관리자입니다.
    """
    def thumbnail(self, obj):
        return mark_safe('<img src="{}" style="width:100px;"/>'.format(obj.file.url))
    thumbnail.short_description = "Thumbnail"
    list_display = ['thumbnail', 'name', 'model_type']
