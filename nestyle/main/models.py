import uuid
from django.db import models


class TempFile(models.Model):
    name = models.CharField(max_length=64)
    created = models.DateTimeField(auto_now=True)
    hash = models.CharField(max_length=64)

class ModelFile(models.Model):
    MODEL_TYPE_CHOICES = (
        ('HAIR', '헤어스타일'),
        ('MODEL', '모델'),
        ('HAIR_MODEL', '헤어스타일 모델'),
        ('CLOTH', '샘플 옷'),
    )
    model_type = models.CharField(
        max_length=32,
        choices=MODEL_TYPE_CHOICES
    ) 
    name = models.CharField('이름', max_length=64)
    file = models.ImageField(upload_to="uploaded/default_files/")
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = models.CharField("모델설명", max_length=1024, default='')

    def __str__(self):
        return '{}({})'.format(self.name, self.model_type)

class UserFile(models.Model):
    file = models.ImageField(upload_to="uploaded/user_files/")
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now=True)