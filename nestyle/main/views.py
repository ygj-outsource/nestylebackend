from django.shortcuts import render
from celery.result import AsyncResult
from django.http import FileResponse, HttpResponse, JsonResponse, Http404
from .tasks import query_fitting, query_hair_change, query_hair_dyeing, request_hair_change_api, request_hair_dyeing_api
from main import models
from django.db.models import F
from django.core import serializers
import json
import uuid
import datetime
import os
from product import models as product_models
from django.views.decorators.csrf import csrf_exempt
import hashlib
from nestyle.celery_app import app as celery_app
from django.core.cache import cache
import logging
import colorsys

logger = logging.getLogger(__name__)

def md5_for_file(f, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.digest()
def md5_for_str(s):
    md5 = hashlib.md5()
    md5.update(s.encode('utf-8'))
    return md5.digest()
@csrf_exempt
def api_index(request):
    upload_id = request.GET['model_id'] #uuid.uuid4() # one_time_upload에서 준 upload_id
    product_id = request.GET['param'] #uuid.uuid4() # one_time_upload에서 준 upload_id
    request_type = request.GET['request_type']
    # task.args
    maybe_image = cache.get(md5_for_str(f"('{upload_id}', '{product_id}')"), None)
    logger.info(f"cache result : {maybe_image}")


    if maybe_image: result = {"result": maybe_image}
    else:

        if request_type == 'fitting':
            res = query_fitting.delay(upload_id, product_id)
        elif request_type == 'hair_dyeing':
            #color_id = int(product_id)
            rgb_code = product_id
            res = query_hair_dyeing.delay(upload_id, rgb_code)
            #request_hair_dyeing_api(upload_id, color, degree)
        elif request_type == 'hair_changing':
            res = query_hair_change.delay(upload_id, product_id)
            # request_hair_change_api(upload_id, product_id)

        ai_result = res.get() if res.ready() else None
        result = {"result": ai_result}
        if ai_result is None:
            result = {"task_id": res.task_id}

        # i = celery_app.control.inspect()
        # current = i.active()
        # queue = i.scheduled()
        # result['current'] = i.active()
    return JsonResponse(result)


@csrf_exempt
def download_product_image(request, product_id):
    product = product_models.Product.objects.get(id = product_id)
    return FileResponse(product.image)

@csrf_exempt
def one_time_upload(request):
    f = request.FILES['file']
    t = request.GET.get('upload_type', None)
    name = str(uuid.uuid4())

    if t == 'user_upload':
        r = models.UserFile.objects.create(file=f)
        return JsonResponse({"upload_id": name})

    with open('tempfiles/' + name, "wb+") as tf:
        for chunk in f.chunks():
            tf.write(chunk)
    with open('tempfiles/' + name, "rb") as tf:
        h = md5_for_file(tf)
        try:
            r = JsonResponse({"upload_id": models.TempFile.objects.get(hash = h).name, "cached":True})
            os.unlink('tempfiles/' + name)
            logger.info("cache hit! hash : {}".format(h))
            return r
        except:
            models.TempFile.objects.create(name=name, hash = h)
    return JsonResponse({"upload_id": name})

@csrf_exempt
def one_time_download(request):
    name = request.GET.get('upload_id', None)
    is_delete = request.GET.get('is_delete', False)

    try:
        return download_product_image(request, name)
    except:
        pass
    try:
        obj = models.ModelFile.objects.get(uuid=name)
        return FileResponse(obj.file)
    except:
        pass

    try:
        obj = models.UserFile.objects.get(uuid=name)
        return FileResponse(obj.file)
    except:
        pass

    try:
        obj = models.TempFile.objects.get(name=name)
    except:
        raise Http404("404")
    with open('tempfiles/' + name, "rb") as f:
        ret = f.read()
    if is_delete == 'y':
        os.unlink('tempfiles/' + name)
        obj.delete()
    response = HttpResponse(ret, content_type='application/octet-stream')
    response['Content-Disposition'] = f'attachment; filename="{name}"'
    return response

def query_queue_status(request):
    # i = celery_app.control.inspect()
    # current = i.active()
    # queue = i.scheduled()
    return JsonResponse({
        'current': current,
        'queue': queue
    })

# Create your views here.
def check(request, task_id):
    task = AsyncResult(task_id)
    
    result = {"result":task.get() if task.ready() else None}
    if result["result"]:
        
        logger.info("cache for {} (key : {})".format(result["result"], task.args))
        cache.set(md5_for_str(task.args), result["result"])
    # i = celery_app.control.inspect()
    # current = i.active()
    # queue = i.scheduled()

    # result['current'] = i.active()
    # result['queue'] = i.scheduled()

    return JsonResponse(result) 
