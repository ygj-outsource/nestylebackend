import time
import requests
from PIL import Image


from nestyle.celery_app import app as celery_app
import os
import colorsys
import uuid
backend_endpoint = os.environ.get("BACKEND_ENDPOINT")
hair_dying_api_endpoint = os.environ.get("HAIR_DYING_API_ENDPOINT")
hair_style_api_endpoint = os.environ.get("HAIR_STYLE_API_ENDPOINT")
fitting_api_endpoint = os.environ.get("FITTING_API_ENDPOINT")
def simple_merge_image(img1_path, img2_path):
    img1 = Image.open(img1_path)
    img2 = Image.open(img2_path)
    fname = 'tempfiles/' + str(uuid.uuid4())
    img2.resize(img1.size)
    mask = Image.new("L", img1.size, 128)
    
    Image.composite(img1, img2, mask).save(fname, 'png')
    return fname

def request_fitting(model_path, cloth_path):
    with open(model_path, 'rb') as mf:
        with open(cloth_path, 'rb') as cf:
            headers = {
                'key':'none'
            }
            files = {
                'human_image': mf,
                'cloth_image': cf
            }
            response = requests.post(fitting_api_endpoint, headers=headers, files=files)
            url = ''
            print(response.text)

            result = response.json()
            print('fitting result : ', result)
            try:
                url = result['output_url']
            except:
                print('error : ' + response.text)
                
            return download(url)


def request_hair_change_api(model_path, hair_path):
    with open(model_path, 'rb') as mf:
        with open(hair_path, 'rb') as cf:
            headers = {
                'key':'none'
            }
            files = {
                'face_image': model_path,
                'hair_image': hair_path
            }
            response = requests.post(hair_style_api_endpoint, headers=headers, files=files)
            output_url = response.json()['output_url']
            return download(output_url)


def request_hair_dyeing_api(model_path, target_color, target_degree):
    with open(model_path, "rb") as m:
        headers = {
            'key':'none'
        }
        data = {
            'target_color':target_color,
            'target_degree': target_degree
        }
        file = {
            'input_image': m
        }
        resp = requests.post(hair_dying_api_endpoint, headers=headers, files=file, data=data)
        print(resp.json())
        url = resp.json()['rough_url']
        return download(url)


def upload(f):
    result = requests.post(backend_endpoint + "/api/one_time_upload/", files=
    {
        'file':  open(f, "rb")
    }).json()
    print(f"result : {result}")
    return result['upload_id']


def rgb2hsv(rgbcode):
    # rgbcode : rrggbb
    r = int(rgbcode[0:2], 16)
    g = int(rgbcode[2:4], 16)
    b = int(rgbcode[4:6], 16)
    return colorsys.rgb_to_hsv(r / 255.0, g / 255.0, b / 255.0)

def download(url):
    fname = 'tempfiles/' + str(uuid.uuid4())
    if url[0] == '/':
        url = backend_endpoint + url
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(fname, "wb") as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)
    return fname

@celery_app.task
def query_fitting(upload_id, product_id):
    model_image_path = download("/api/one_time_download/?upload_id=" + upload_id)
    try:
        int(product_id)
        cloth_path = download("/api/product_image/" + product_id)
    except:
        cloth_path = download("/api/one_time_download/?upload_id=" + product_id)
    print(f"model_image_path : {model_image_path} and cloth_path : {cloth_path}")
    #result_file = request_fitting(model_image_path, cloth_path)
    result_file = simple_merge_image(model_image_path, cloth_path)
    #time.sleep(3)
    # result_file = model_image_path
    r = upload(result_file)
    os.unlink(model_image_path)
    os.unlink(cloth_path)
    os.unlink(result_file)
    return r

@celery_app.task
def query_hair_dyeing(upload_id, rgb_code):
    hue = 180
    saturation = 120
    if rgb_code == '000000':
        hue = 180
        saturation = 120
    elif rgb_code == 'ffffff':
        hue = 179
        saturation = 0
    else:                
        #rgb_code = rgb_code[1:]
        hsv = rgb2hsv(rgb_code)
        hue = int(180 * hsv[0])
        saturation = int(120 * hsv[1])
        value = hsv[2]
    # https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=samsjang&logNo=220504633218
    color = hue
    degree = saturation

    model_image_path = download("/api/one_time_download/?upload_id=" + upload_id)

    result_file = request_hair_dyeing_api(model_image_path, color, degree)
    r = upload(result_file)
    os.unlink(model_image_path)
    os.unlink(result_file)
    return r

@celery_app.task
def query_hair_change(upload_id, hair_url):
    size = 512, 512
    model_image_path = download("/api/one_time_download/?upload_id=" + upload_id)
    hair_image_path = download(hair_url)
    Image.open(model_image_path).resize(size, Image.ANTIALIAS).save(model_image_path, 'JPEG')
    Image.open(hair_image_path).resize(size, Image.ANTIALIAS).save(hair_image_path, 'JPEG')
    
    result_file = request_hair_change_api(model_image_path, hair_image_path)
    r = upload(result_file)
    os.unlink(model_image_path)
    os.unlink(hair_image_path)
    return r


@celery_app.task
def check_queue():
    pass