FROM python:3.9-alpine3.13

# Install dependencies required for psycopg2 python package
RUN apk update && apk add libpq
RUN apk update && apk add --virtual .build-deps gcc python3-dev musl-dev postgresql-dev 
RUN apk update && apk add jpeg-dev zlib-dev libjpeg
RUN apk update && apk add nginx
RUN mkdir -p /run/nginx

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY nestyle nestyle
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Remove dependencies only required for psycopg2 build
RUN apk del .build-deps
EXPOSE 8080
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY run_server.sh /usr/src/app/run_server.sh
RUN chmod 777 /usr/src/app/run_server.sh
CMD ["run_server.sh"]